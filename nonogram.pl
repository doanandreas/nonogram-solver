%%
%   generate(Combo, Constraint, Size):
%   Generates all possible configuration (combos)
%   of the given set of rows/columns constraints & size.
%%
generate(Combinations, Constraint, Size) :- generate_rec(1, Combinations, Constraint, Size).

%%
%   generate_rec(LineNumber, Combo, Constraint, Size):
%   Recursive helper for generate/3.
%%
generate_rec(_, [], [], _).
generate_rec(LineNumber, [line(LineNumber, Combinations) | TailCombinations], [HeadConstraint|TailConstraint], Size) :-
    findall(Combination, combination(HeadConstraint, Size, Combination), Combinations),
    NextLineNumber is LineNumber + 1,
    generate_rec(NextLineNumber, TailCombinations, TailConstraint, Size).

%%
% generate_overlap(LineCombo, LineOverlap, Fill, Empty):
% get a list consist of block number in each line that will be Filled (fix block)
%%
generate_overlap([],[],_,_).
generate_overlap([line(Number, Combo) | TailCombo], [line(Number, Overlap) | TailOverlap], 
                 Fill, Empty):-
    length(Fill, LengthFill),
    length(Empty, LengthEmpty),
    LengthFill = 0,
    LengthEmpty = 0,
    constrainCombo(Combo, Fill, Empty, ConstrainCombo),
    findOverlap(ConstrainCombo, Overlap),
    generate_overlap(TailCombo, TailOverlap, Fill, Empty),
    !.
generate_overlap([line(Number, Combo) | TailCombo], [line(Number, Overlap) | TailOverlap], 
                 [line(Number, Fill)|NextFill], [line(Number, Empty)|NextEmpty]):-
    constrainCombo(Combo, Fill, Empty, ConstrainCombo),
    findOverlap(ConstrainCombo, Overlap),
    generate_overlap(TailCombo, TailOverlap, NextFill, NextEmpty).

%% Finds sum of all elements of list.
sum(0, []).
sum(Total, [Head | Tail]) :- sum(Temp, Tail), Total is Temp + Head.

%% Generates number from Start to Max.
cabang_mulai(Start, Start, _).
cabang_mulai(Start, StartBlock, MaxStart) :- 
    StartBlock < MaxStart, 
    NewStartBlock is StartBlock + 1, 
    cabang_mulai(Start, NewStartBlock, MaxStart).

%% Generates a list containing number from Start to End.
buat_kombinasi([End], End, End) :- !.
buat_kombinasi(Combination, Start, FinishBlock) :- 
    Start < FinishBlock, 
    NewStart is Start + 1, 
    buat_kombinasi(NewCombination, NewStart, FinishBlock), 
    append([Start], NewCombination, Combination).

%%
%   combination(Constraint, Size, Combo)
%   Generates all possible combination (combo)
%   for a given row/column.
%%
combination(Constraints, Size, Combinations) :-
    sum(TotalBlocks, Constraints),
    length(Constraints, Length),
    CortegeLength is TotalBlocks + Length - 1,
    combination_support(Constraints, 1, CortegeLength, Size, [], Combinations).

%%
%   combination_support(Constraint, Start, CortegeLength, Size, PrevCombo, Combo)
%   Supporting recursive predicate for combination/3.
%%
combination_support([Number], StartBlock, _, Size, PrevCombination, Combinations) :-
    MaxStart is Size - Number + 1,
    cabang_mulai(Start, StartBlock, MaxStart),
    FinishBlock is Number + Start - 1,
    buat_kombinasi(Combination, Start, FinishBlock),
    append(PrevCombination, Combination, Combinations).
combination_support([Number|Tail], StartBlock, CortegeLength, Size, PrevCombination, Combinations) :-
    MaxStart is Size - CortegeLength + 1,
    cabang_mulai(Start, StartBlock, MaxStart),
    FinishBlock is Number + Start -1,
    buat_kombinasi(Combination, Start, FinishBlock),
    NewCortegeLength is CortegeLength - 1 - Number,
    append(PrevCombination, Combination, NewCombination),  
    combination_support(Tail, Start + Number, NewCortegeLength, Size, NewCombination, Combinations).

%%
%   findOverlap(ListCombo, Overlap):
%   Finds intersection of all combos.
%%
findOverlap([Head], Head) :- !.
findOverlap([Head|Tail], Overlap) :- 
    findOverlap(Tail, PrevOverlap), intersect(Head, PrevOverlap, Overlap), !.

% intersect(List1, List2, Result):
% Finds intersection of 2 lists.
intersect([], _, []):-!.
intersect([Head1|Tail1], List2, Result) :-
    member(Head1, List2), Result = [Head1|NextRes], intersect(Tail1, List2, NextRes), !;    
    intersect(Tail1, List2, Result).

%%
%   constrainCombo(Combo, Fill, Empty, Solution):
%   Finds combo that adheres to filled and emptied position.
%%
constrainCombo([],_,_,[]).
constrainCombo([Combo|NextCombo], Fill, Empty, Sol) :-
    intersect(Combo, Fill, FillIntr), FillIntr = Fill,
    intersect(Combo, Empty, EmptyIntr), EmptyIntr = [],
    Sol = [Combo|NextSol], constrainCombo(NextCombo, Fill, Empty, NextSol), !;
    constrainCombo(NextCombo, Fill, Empty, Sol).    

% Contoh nonogram.
% nonogram(NamaNonogram, Row, Column).
% Cara pakai: solveNonogram(NamaNonogram).
nonogram(duck,
    [[3],[2,1],[3,2],[2,2],[6],[1,5],[6],[1],[2]],
    [[1,2],[3,1],[1,5],[7,1],[5],[3],[4],[3]]).
nonogram(huruf_p,
    [[4],[6],[2,2],[2,2],[6],[4],[2],[2],[2]],
    [[9],[9],[2,2],[2,2],[4],[4]]).
nonogram(dog,
    [[1],[2],[1,3],[5],[5],[2,2],[2,1,1],[1,1]],
    [[1,1],[5],[3],[2],[2],[8],[2,1],[1,1]]).
nonogram(something,                         
    [[5],[1,6],[3,1],[4,1],[3,1],[9,2],[8],[8,1,1],[1,4],[1,2,3,1],[4,6],[1,1,6],[1,9],[3,3],[10,3]],
    [[1,1,1],[1,1],[2,1],[3,1,1],[1,5,4,1],[11,1],[8,5],[4,3,3],[2,3,3],[2,8,1],[6],[5],[2,5],[1,5],[4,1,6]]).

%% Nonogram dengan strategi lebih efisien.
solveNonogram(RowsConstraint, ColsConstraint) :- 
    length(RowsConstraint, Vsize),
    length(ColsConstraint, Hsize),
    generate(RowsCombination, RowsConstraint, Hsize),
    generate(ColsCombination, ColsConstraint, Vsize),
    solve_rec(RowsConstraint, ColsConstraint, Vsize, Hsize, RowsCombination, ColsCombination, [], [], [],[],Solution),
    printSolution(Solution).
solveNonogram(Nonogram) :-
    nonogram(Nonogram, Row, Col),
    solveNonogram(Row, Col).

% while loop to search the solution
solve_rec(_,_,Vsize, Hsize, _,_, FixFilledRow, FixEmptyRow, FixFilledCol, FixEmptyCol, Solution) :-
    checkComplete(FixFilledRow, FixEmptyRow, Hsize),
    checkComplete(FixFilledCol, FixEmptyCol, Vsize),
    Solution = FixFilledRow,
    !.
solve_rec(RowsConstraint, ColsConstraint, Vsize, Hsize, RowsCombination, ColsCombination, 
        FixFilledRow, FixEmptyRow, FixFilledCol, FixEmptyCol,Solution) :-
    generate_overlap(RowsCombination, RowOverlap, FixFilledRow, FixEmptyRow),
    generate_overlap(ColsCombination, ColOverlap, FixFilledCol, FixEmptyCol),
    union(RowOverlap, ColOverlap, FixFilledDariRow),
    union(ColOverlap, RowOverlap, FixFilledDariCol),
    findEmptyRow(FixFilledDariRow, RowsConstraint, NextFixEmptyRow, FixEmptyRow, Hsize),
    findEmptyRow(FixFilledDariCol, ColsConstraint, NextFixEmptyCol, FixEmptyCol, Vsize),
    union(NextFixEmptyRow, NextFixEmptyCol, FixEmptyDariRow),
    union(NextFixEmptyCol, NextFixEmptyRow, FixEmptyDariCol),
    solve_rec(RowsConstraint, ColsConstraint, Vsize, Hsize, RowsCombination, ColsCombination,
        FixFilledDariRow, FixEmptyDariRow, FixFilledDariCol, FixEmptyDariCol, Solution).

% checkComplete([],[],_).
checkComplete([Fill], [Empty], Size) :- 
    Fill = line(Num, FillList), Empty = line(Num, EmptyList),
    length(FillList, PanjangFill), length(EmptyList, PanjangEmpty),
    TotalLength is PanjangFill + PanjangEmpty, TotalLength = Size.
checkComplete([Fill|NextFill], [Empty|NextEmpty], Size) :- 
    Fill = line(Num, FillList), Empty = line(Num, EmptyList),
    length(FillList, PanjangFill), length(EmptyList, PanjangEmpty),
    TotalLength is PanjangFill + PanjangEmpty, TotalLength = Size,
    checkComplete(NextFill, NextEmpty, Size).

%print solution of nanogram by calling printLine/2
printSolution([]).
printSolution([line(_, List)|T]) :- printLine(List, 1), nl, printSolution(T).

%print solution of nth line of nanogram
printLine([], _) :- !.
printLine([H|T], ColNumber) :- ColNumber < H, write('  '), NewColNumber is ColNumber + 1, !, printLine([H|T], NewColNumber).
printLine([H|T], ColNumber) :- ColNumber = H, write('o '), NewColNumber is ColNumber + 1, printLine(T, NewColNumber).

%get union of list
union([],_,[]).
union([HRowComb|TRowComb], ColComb, [line(Number, FilledBox)|TFilledBox]) :-
    unionLine(HRowComb, ColComb, Number, FilledBox),
    union(TRowComb, ColComb, TFilledBox).

unionLine(_,[],_,[]).
unionLine(line(RowNumber,[]),[line(ColNumber, ColComb)|TColComb], RowNumber, FilledBox) :-
    member(RowNumber, ColComb),
    unionLine(line(RowNumber, []), TColComb, RowNumber, PrevFilledBox),
    append([ColNumber], PrevFilledBox, FilledBox),
    !.
unionLine(line(RowNumber,[]),[line(_, _)|TColComb], RowNumber, FilledBox) :-
    unionLine(line(RowNumber, []), TColComb, RowNumber, FilledBox),
    !.
unionLine(line(RowNumber, [HRow|TRow]), [line(ColNumber, ColComb)|TColComb], RowNumber, FilledBox) :-
    HRow \= ColNumber,
    member(RowNumber, ColComb),
    unionLine(line(RowNumber, [HRow|TRow]), TColComb, RowNumber, PrevFilledBox),
    append([ColNumber], PrevFilledBox, FilledBox),
    !.
unionLine(line(RowNumber, [HRow|TRow]), [line(ColNumber, _)|TColComb], RowNumber, FilledBox) :-
    HRow \= ColNumber,
    unionLine(line(RowNumber, [HRow|TRow]), TColComb, RowNumber, FilledBox),
    !.
unionLine(line(RowNumber, [HRow|TRow]), [line(ColNumber, _)|TColComb], RowNumber, FilledBox) :-
    HRow = ColNumber,
    unionLine(line(RowNumber, TRow), TColComb, RowNumber, PrevFilledBox),
    append([HRow], PrevFilledBox, FilledBox).

%find block number of nanogram that will be empty (fix)
findEmptyRow([] , [], [],[],_).
findEmptyRow([line(RowNumber, FilledBox) | TComb], [HConstraint|TConstraint], 
[line(RowNumber, EmptyBox) | TLineEmpty], [], Hsize) :-
    findEmpty(FilledBox, HConstraint, EmptyBox, [], Hsize),
    findEmptyRow(TComb, TConstraint, TLineEmpty, [], Hsize),
    !.
findEmptyRow([line(RowNumber, FilledBox) | TComb], [HConstraint|TConstraint], 
[line(RowNumber, EmptyBox) | TLineEmpty], [line(RowNumber, PrevEmptyBox) | TPrevEmpty], Hsize) :-
    findEmpty(FilledBox, HConstraint, EmptyBox, PrevEmptyBox, Hsize),
    findEmptyRow(TComb, TConstraint, TLineEmpty, TPrevEmpty, Hsize).

%find empty block in each line of Nanogram
findEmpty(FilledBox,HConstraint,EmptyBox, PrevEmpty, Hsize) :-
    findall(Combination, combination(HConstraint, Hsize, Combination), Combinations),
    constrainCombo(Combinations, FilledBox, PrevEmpty, ConstrainCombo),
    findOverlap(ConstrainCombo, _),
    appendCombo(ConstrainCombo, AllCom),
    list_to_set(AllCom, UnionCom),
    searchEmpty(1, UnionCom, Hsize, EmptyBox).

%append all combination of nth line Nanogram
appendCombo([],[]).
appendCombo([H|T],UnionCom) :-
    appendCombo(T, PrevUnionCom),
    append(H, PrevUnionCom, UnionCom).

%get list of number that not exist in UnionCom
searchEmpty(Number, _, Size, []) :- Number > Size, !.
searchEmpty(Number, UnionCom, Size, Solution) :-
    NewNumber is Number + 1,
    searchEmpty(NewNumber, UnionCom, Size, PrevSolution),
    \+member(Number, UnionCom),
    append([Number], PrevSolution, Solution), !.
searchEmpty(Number, UnionCom, Size, Solution) :-
    NewNumber is Number + 1,
    searchEmpty(NewNumber, UnionCom, Size, Solution).
    

   
